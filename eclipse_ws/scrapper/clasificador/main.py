import os
import shutil

root = r"/home/mati/Documents/Materias/AprendizajeAut/Dataset"
all_dir = r"/home/mati/Documents/Materias/AprendizajeAut/All"
try:
    os.makedirs(all_dir, exist_ok = True)
    print("Directory '%s' created successfully" % all_dir)
except OSError as error:
    print("Directory '%s' can not be created" % all_dir)
    
audioFilesSample = []
y = []
tag = 0
for subdir, dirs, files in os.walk(root):
    #dire = subdir.split("/")[-1]
    for file in files:
        path_file = os.path.join(root,subdir)  
        path_file = os.path.join(path_file,file)
        audioFilesSample.append("All/"+file)
        y.append(tag)
        #all_dir = os.path.join(parent_dir, "All")
        shutil.copy2(path_file, all_dir)
    tag += 1
    
import numpy as np
import librosa
import librosa.display
import matplotlib.pyplot as plt
import IPython.display as ipd

import pandas as pd
import altair as alt

import utils

audioData = utils.getAudioData( list(audioFilesSample), "stft", audioMaxLength = 1 )
audioDataTransformed = utils.doPCA(audioData)

utils.doDistanceMatrix( audioDataTransformed )
tsne = utils.doTSNE( audioDataTransformed, 2 )
tsne.shape

dataset = pd.DataFrame({'TSNE_1': tsne[:, 0], 'TSNE_2': tsne[:, 1], "Filename" : audioFilesSample })

alt.Chart(dataset, width=800, height=500).mark_circle().encode(
    x="TSNE_1",
    y="TSNE_2",
    tooltip=['Filename']
).interactive()

from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB
X = tsne
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.5, random_state=0)
gnb = GaussianNB()
y_pred = gnb.fit(X_train, y_train).predict(X_test)
print("Number of mislabeled points out of a total %d points : %d"
       % (X_test.shape[0], (y_test != y_pred).sum()))


from sklearn.neighbors import (NeighborhoodComponentsAnalysis,KNeighborsClassifier)
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
X = tsne
X_train, X_test, y_train, y_test = train_test_split(X, y, stratify =y, test_size=0.7, random_state=42)
nca = NeighborhoodComponentsAnalysis(random_state=42)
knn = KNeighborsClassifier(n_neighbors=3)
nca_pipe = Pipeline([('nca', nca), ('knn', knn)])
nca_pipe.fit(X_train, y_train)
#Pipeline(...)
print(nca_pipe.score(X_test, y_test))


