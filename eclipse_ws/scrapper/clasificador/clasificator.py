'''
Created on Jun 28, 2022

@author: seb
'''

import module_datasetzoid as ds

# def ReportSamples(categories, category_list):
    # print(('%16s | %16s') % ('Category', 'Size'))
    # for cat_id in range(len(categories)):
        # print(('%16s | %16s') % (categories[cat_id].name, category_list.count(cat_id)))
    # print(('%16s | %16s') % ('Total', len(category_list)))

if __name__ == '__main__':
    categories = list()
    categories.append(ds.Category('Bass',      ['bass']))
    categories.append(ds.Category('Tom',       ['tom', 'conga', 'timbale']))
    categories.append(ds.Category('Cymbal',    ['cymbal', 'hat']))
    categories.append(ds.Category('Fx',        ['fx', 'click']))
    categories.append(ds.Category('Snaredrum', ['snare']))
    
    datasetzoid = ds.Datasetzoid('dataset', categories, 'drumkits')
    
    file_list, category_list = datasetzoid.SamplesGet()
    
    print(category_list)
    print(file_list)
    ds.ReportSamples(categories, category_list)
    
    file_list, category_list = datasetzoid.SamplesGet(20)
    
    print(category_list)
    print(file_list)
    ds.ReportSamples(categories, category_list)
    
    # print("Inicio")
    # print("#file_list:", len(file_list))
    # print("Filtrado ...")
    # for word in synonymous_list:
    #     print("word:", word)
    #     filtered_files_list = FileNameFilter(file_list, [word])
    #     print("filtered_files_list:", filtered_files_list)
    #     print("#filtered_files_list:", len(filtered_files_list))
    #     file_list = list(set(file_list) - set(filtered_files_list))
    #     print("file_list:", file_list)
    #     print("#file_list:", len(file_list))
    # print("Filtrado ... OK")
    
    # path_root = r"drumkits"
    # file_list = MP3FileList(path_root) 
    #
    # parent_dir = r"dataset"
    # #directorys = ["Bass", "Tom", "Clap", "Tambourine", "Whistle", "Conga", "CYMBAL", "SFX", "Snaredrum"]
    # # En vez de tomar las 9 clases, tomo sólo 6, porque las otras 3 tienen muy pocas muestras, y no se alcanza a hacer un buen entrenamiento. 
    # # Dejo comentado lo que usa las 9 clases por las dudas
    # directorys = ["Bass", "Tom", "Conga", "CYMBAL", "SFX", "Snaredrum"]
    # root = r"drumkits.mp3"
    #
    # try:
    #     os.makedirs(parent_dir, exist_ok = True)
    #     print("Directory '%s' created successfully" % parent_dir)
    # except OSError as error:
    #     print("Directory '%s' can not be created" % parent_dir)
    # for i in range(len(directorys)):   
    #     path = os.path.join(parent_dir, directorys[i])
    #     try:
    #         os.makedirs(path, exist_ok = True)
    #         print("Directory '%s' created successfully" % directorys[i])
    #     except OSError as error:
    #         print("Directory '%s' can not be created" % directorys[i])
    #
    # ## otra seccion
    # for subdir, dirs, files in os.walk(root):
    #     for file in files:
    #         if (file.split('.')[-1]=="mp3"):
    #           path_file = os.path.join(subdir,file)
    #           # Tenia el problema que habia archivos de mas de 1 segundo que despues tiramos
    #           # y que al tirar no sabemos el indice para sacarlo luego de la lista de tags
    #           if(os.path.getsize(path_file)<30000):
    #             if ("Bass" in file) or ("bass" in file):            
    #                 dir_path = os.path.join(parent_dir, directorys[0])
    #                 shutil.copy(path_file, dir_path)
    #             elif ("Tom" in file) or ("tom" in file) or ("TOM" in file):
    #                 dir_path = os.path.join(parent_dir, directorys[1])
    #                 shutil.copy2(path_file, dir_path)    
    #             elif ("Conga" in file) or ("conga" in file):
    #                 dir_path = os.path.join(parent_dir, directorys[2])
    #                 shutil.copy2(path_file, dir_path)           
    #             elif ("CYMBAL" in file):
    #                 dir_path = os.path.join(parent_dir, directorys[3])
    #                 shutil.copy2(path_file, dir_path)  
    #             elif ("SFX" in file):
    #                 dir_path = os.path.join(parent_dir, directorys[4])
    #                 shutil.copy2(path_file, dir_path)  
    #             elif ("Snaredrum" in file) or ("snaredrum" in file):
    #                 dir_path = os.path.join(parent_dir, directorys[5])
    #                 shutil.copy2(path_file, dir_path)
    #
    # root = r"dataset"
    # all_dir = r"All"
    # try:
    #     os.makedirs(all_dir, exist_ok = True)
    #     print("Directory '%s' created successfully" % all_dir)
    # except OSError as error:
    #     print("Directory '%s' can not be created" % all_dir)
    #
    # audioFilesSample = []
    # y = []
    # tag = 0
    # for subdir, dirs, files in os.walk(root):
    #     #dire = subdir.split("/")[-1]
    #     for file in files:
    #         path_file = os.path.join(subdir,file)
    #         audioFilesSample.append("All/"+file)
    #         y.append(tag)
    #         #all_dir = os.path.join(parent_dir, "All")
    #         shutil.copy2(path_file, all_dir)
    #     tag += 1
    # len(audioFilesSample)
                    
                    
                    
                    
                    