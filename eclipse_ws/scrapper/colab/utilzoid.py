import librosa
import numpy as np
import os
from math import ceil
import time
import sys

def getSTFT(data, superVector = True):
    D = librosa.stft(data)
    D = np.abs(D)
    if superVector:
        return D.reshape(D.shape[0] * D.shape[1])
    else:
        return D.reshape((D.shape[1], D.shape[0]))

def getAudioData(audioFiles, superVector = True, features = "stft", audioMaxLength = 3, qtyFilesToProcess = None):
    count = 0
    countFail = 0
    COUNT_NOTICE = 200
#    COUNT_FAIL = 20

    maxProgress = 0.5
    listAudioData = []
    tic = time.clock()
    audioFilesDone = []
    sizeAudioRaw = ceil(SAMPLE_RATE * audioMaxLength)

    if qtyFilesToProcess == None:
        qtyFilesToProcess = len(audioFiles)

    for i in range(0, qtyFilesToProcess):
        try:
            file = audioFiles[i]
            sys.stdout.write('.')
            sys.stdout.flush()

            # tmpAudioData, tmpSampleRate = librosa.core.load(file, sr = SAMPLE_RATE)
            # tmpAudioData.resize(sizeAudioRaw)
            tmpAudioData, tmpSampleRate = AudioLoad(file)

            featuresData = None

            if features == "mfcc":
                featuresData = getMFCC(tmpAudioData, superVector)
            elif features == "stft":
                featuresData = getSTFT(tmpAudioData)

            listAudioData.append(featuresData)
            audioFilesDone.append(file)

            count += 1

            if count % COUNT_NOTICE == 0:
                sys.stdout.write('\n\r')
                print("[", count, "/", qtyFilesToProcess, "]")
                sys.stdout.flush()

        except Exception as ex:
            countFail += 1
            sys.stdout.write('\n\r')
            print(file, "[FAIL]", ex)
            sys.stdout.flush()

            

            # if countFail >= COUNT_FAIL:
            #     break

    matrixAudioData = np.array(listAudioData, dtype=np.float32)
#    matrixAudioData = matrixAudioData.squeeze(1)
    audioFiles.clear()
    audioFiles += audioFilesDone

    print("")
    print("Matriz final:", matrixAudioData.shape)

    toc = time.clock()
    print("time:", toc - tic)
    return matrixAudioData, audioFilesDone


def AudioLoad(file_name):
    sizeAudioRaw = ceil(SAMPLE_RATE * AUDIO_NORMALIZE_LENGTH)
    wave, sr = librosa.load(file_name, sr = SAMPLE_RATE)
    wave.resize(sizeAudioRaw)
    return wave, sr

def AudioShow(files):
    for idx in range(len(files)):
        file_name = files[idx]
        title = 'cat: %d, file: %s' %(idx, file_name)

        display(title)
        display(ipd.Audio(file_name))

def WaveShow(files):
    plt.figure(figsize=(10, 5))
    for idx in range(len(files)):
        file_name = files[idx]
        title = 'cat: %d, file: %s' %(idx, file_name)

        plt.subplot(1, len(categories), idx + 1)

        # audioMaxLength = 5
        # sizeAudioRaw = ceil(SAMPLE_RATE * audioMaxLength)
        # wave, sr = librosa.load(file_name, sr = SAMPLE_RATE)
        # wave.resize(sizeAudioRaw)
        wave, sr = AudioLoad(file_name)
        
        plt.title(title)
        plt.plot(wave)

def SpectrumShow(files):
    plt.figure(figsize=(10, 5))
    for idx in range(len(files)):
        file_name = files[idx]
        title = 'cat: %d, file: %s' %(idx, file_name)

        plt.subplot(1, len(categories), idx + 1)

        wave, sr = AudioLoad(file_name)

        waveSTFT = librosa.stft(wave)
        wavedb = librosa.amplitude_to_db(abs(waveSTFT))
        librosa.display.specshow(wavedb, sr=sr, x_axis='time', y_axis='log')

        plt.title(title)
        plt.colorbar()

def SignalShow(_categories):
    files = []
    for cat in _categories:
        files.append(cat.files_list[0])

    AudioShow(files)
    WaveShow(files)
    SpectrumShow(files)

