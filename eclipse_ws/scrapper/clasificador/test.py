'''
Created on Jun 28, 2022

@author: seb
'''

import os
import shutil

class Category:
    def __init__(self, name, synonymous_list):
        self.name = name
        self.synonymous_list = synonymous_list
        self.files_list = list()

def MP3FileList(path_root):
    files_list = list()
    for root, _, files in os.walk(path_root,):
        for name in files:
            file_path = os.path.join(root, name)
            file_path = file_path.lower()
            if(-1 != file_path.find('mp3')):
                files_list.append(file_path)
    return files_list

def FileNameFilter(files_list, synonymous_list):
    filtered_files_list = list()
    for file_path in files_list:
        for synonym in synonymous_list:
            if(-1 != file_path.find(synonym)):
                filtered_files_list.append(file_path)
                break
    return filtered_files_list

if __name__ == '__main__':
    path_root = r"drumkits"
    file_list = MP3FileList(path_root)
    print("file_list:", file_list)
    print("#file_list:", len(file_list))
    
    synonymous_list = [
        'timbale',
        'bass',
        'tom',
        'clap',
        'tambourine',
        'whistle',
        'conga',
        'cymbal',
        'fx',
        # 'sfx',
        'snare',
        # 'snaredrum',
        'click',
        'hat',
        'misc',
        'cowbell',
        'agogo',
        'percussion',
        'percusson',
        'clave',
        'rim',
        'triangle',
        ]
    
    print("Inicio")
    print("#file_list:", len(file_list))
    print("")
    print("Filtrado ...")
    for word in synonymous_list:
        print("")
        print("word:", word)
        filtered_files_list = FileNameFilter(file_list, [word])
        print("filtered_files_list:", filtered_files_list)
        print("#filtered_files_list:", len(filtered_files_list))
        file_list = list(set(file_list) - set(filtered_files_list))
        print("file_list:", file_list)
        print("#file_list:", len(file_list))
        print("")
    print("")
    print("Filtrado ... OK")
    
    
    
    
    # cnt_files = 0
    # for root, dirs, files in os.walk(root,):
    #     for name in files:
    #         file_path = os.path.join(root, name)
    #         # print('file_path:', file_path)
    #         cnt_files += 1
    #
    # print("cnt_files:", cnt_files)
    #     # for name in dirs:
    #     #     print(os.path.join(root, name))
