import os
import shutil
'''
TODO:
- que si un archivo ya existe no lo pise.
- Abstraer el root path para cualquier maquina
- Chequear que catergorias mas queremos o cuales no.
'''
parent_dir = r"/home/mati/Documents/Materias/AprendizajeAut/Dataset"
directorys = ["Bass", "Tom", "Clap", "Tambourine", "Whistle", "Conga", "CYMBAL", "SFX", "Snaredrum"]
root = r"/home/mati/Documents/Materias/AprendizajeAut/drumkits.mp3"

try:
    os.makedirs(parent_dir, exist_ok = True)
    print("Directory '%s' created successfully" % parent_dir)
except OSError as error:
    print("Directory '%s' can not be created" % parent_dir)
    
for i in range(len(directorys)):   
    path = os.path.join(parent_dir, directorys[i])
    try:
        os.makedirs(path, exist_ok = True)
        print("Directory '%s' created successfully" % directorys[i])
    except OSError as error:
        print("Directory '%s' can not be created" % directorys[i])
  
for subdir, dirs, files in os.walk(root):
    for file in files:
        path_file = os.path.join(root,subdir)  
        path_file = os.path.join(path_file,file)
        if ("Bass" in file) or ("bass" in file):            
            dir_path = os.path.join(parent_dir, directorys[0])
            shutil.copy(path_file, dir_path)
        elif ("Tom" in file) or ("tom" in file) or ("TOM" in file):
            dir_path = os.path.join(parent_dir, directorys[1])
            shutil.copy2(path_file, dir_path)
        elif ("Clap" in file) or ("clap" in file):
            dir_path = os.path.join(parent_dir, directorys[2])
            shutil.copy2(path_file, dir_path)
        elif ("Tambourine" in file) or ("tambourine" in file):
            dir_path = os.path.join(parent_dir, directorys[3])
            shutil.copy2(path_file, dir_path)
        elif ("Whistle" in file) or ("whistle" in file):
            dir_path = os.path.join(parent_dir, directorys[4])
            shutil.copy2(path_file, dir_path)      
        elif ("Conga" in file) or ("conga" in file):
            dir_path = os.path.join(parent_dir, directorys[5])
            shutil.copy2(path_file, dir_path)           
        elif ("CYMBAL" in file):
            dir_path = os.path.join(parent_dir, directorys[6])
            shutil.copy2(path_file, dir_path)  
        elif ("SFX" in file):
            dir_path = os.path.join(parent_dir, directorys[7])
            shutil.copy2(path_file, dir_path)  
        elif ("Snaredrum" in file) or ("snaredrum" in file):
            dir_path = os.path.join(parent_dir, directorys[8])
            shutil.copy2(path_file, dir_path)  



root = r"/home/mati/Documents/Materias/AprendizajeAut/Dataset"
all_dir = r"/home/mati/Documents/Materias/AprendizajeAut/All"
try:
    os.makedirs(all_dir, exist_ok = True)
    print("Directory '%s' created successfully" % all_dir)
except OSError as error:
    print("Directory '%s' can not be created" % all_dir)
    
audioFilesSample = []
y = []
tag = 0
for subdir, dirs, files in os.walk(root):
    #dire = subdir.split("/")[-1]
    for file in files:
        path_file = os.path.join(root,subdir)  
        path_file = os.path.join(path_file,file)
        audioFilesSample.append("All/"+file)
        y.append(tag)
        #all_dir = os.path.join(parent_dir, "All")
        shutil.copy2(path_file, all_dir)
    tag += 1
            