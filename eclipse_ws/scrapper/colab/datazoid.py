import os
import sys
import shutil
import random
import copy
import time

import numpy as np
from math import ceil

import librosa
import librosa.display
import matplotlib.pyplot as plt
import IPython.display as ipd

import pandas as pd
import altair as alt

def MP3FileList(path_root):
    files_list = list()
    for root, _, files in os.walk(path_root,):
        for name in files:
            file_path = os.path.join(root, name)
            # file_path = file_path.lower()
            if(-1 != file_path.lower().find('mp3')):
                files_list.append(file_path)
    return files_list

def FileNameFilter(files_list, synonymous_list):
    filtered_files_list = list()
    for file_path in files_list:
        for synonym in synonymous_list:
            if(-1 != file_path.lower().find(synonym)):
                filtered_files_list.append(file_path)
                break
    return filtered_files_list

def ReportSamples(categories, category_list):
    # print('')
    print(('%16s | %16s') % ('Category', 'Size'))
    for cat_id in range(len(categories)):
        print(('%16s | %16s') % (categories[cat_id].name, category_list.count(cat_id)))
    print(('%16s | %16s') % ('Total', len(category_list)))

class Category:
    def __init__(self, name, synonymous_list):
        self.name = name
        self.synonymous_list = synonymous_list
        self.files_list = list()

class Categoryzoid:
    def __init__(self, categories):
        self.categories = categories
    
    def Classify(self, file_list, verbose = False, delete_filtered = False):
        if(verbose):
            print(('%16s | %16s') % ('Category', 'Items'))
        
        file_list = file_list.copy()
        for category in self.categories:
            category.files_list = FileNameFilter(file_list, category.synonymous_list)
            
            if(delete_filtered):
                file_list = list(set(file_list) - set(category.files_list))
            
            if(verbose):
                print(('%16s | %16d') % (category.name, len(category.files_list)))
        
        print('')
        print(('%16s | ') % (''), end='')
        for category in self.categories:
            print(('%16s | ') % (category.name), end='')
        print('')
        
        for src_category in self.categories:
            src_files = src_category.files_list
            print(('%16s | ') % (src_category.name), end='')
            for cmp_category in self.categories:
                cmp_files = cmp_category.files_list
                
                x = set(src_files)
                y = set(cmp_files)
                z = x.intersection(y)
                print(('%16d | ') % (len(z)), end='')
            print('')
    
    def FilesInCategories(self):
        categories = list()
        for category in self.categories:
            categories.append(category.files_list)
        
        return categories
    
    def CopyGenerate(self, root_path):
        categories = self.FilesInCategories()
        categories_file_match = copy.deepcopy(categories)
        for category_id in range(len(categories)):
            for file_id in range(len(categories[category_id])):
                file_src = categories[category_id][file_id]
                file_name = '%03d_%03d.mp3' % (category_id, file_id)
                file_path = os.path.join(root_path, file_name)
                shutil.copy(file_src, file_path)
                categories[category_id][file_id] = file_path
                categories_file_match[category_id][file_id] = file_src

        with open('file_match.txt', 'w') as f:
            for category_id in range(len(categories)):
                for file_id in range(len(categories[category_id])):
                    file_src = categories_file_match[category_id][file_id]
                    file_dst = categories[category_id][file_id]
                    f.write(file_dst + ' > ' + file_src + '\r\n')
        
        return categories, categories_file_match

class Dataset:
    def __init__(self, categories, category_list, file_list, file_source_list):
        self.categories = categories       
        self.category_list = category_list
        self.file_list = file_list
        self.file_source_list = file_source_list
    
    # def NewDataset_Filter_bySize(self, max_size):
    #     return Dataset(categories)

    def _CategorySize(self):
        category_size_list = list()
        for category_id in range(len(self.categories)):
            category_size_list.append(self.category_list.count(category_id))
        return category_size_list

    def _List_Filter(self, list_src, filter_list):
        list_dst = list()
        for idx in range(len(filter_list)):
            if(1 == filter_list[idx]):
                list_dst.append(list_src[idx])
        return list_dst

    def NewDataset_Filter(self, filter_list):
        category_list = self._List_Filter(self.category_list, filter_list)
        file_list = self._List_Filter(self.file_list, filter_list)
        file_source_list = self._List_Filter(self.file_source_list, filter_list)
        newDataset = Dataset(self.categories, category_list, file_list, file_source_list)
        return newDataset

    def NewDataset_Filter_byCategorySize(self, category_max_size):
        category_size_list = self._CategorySize()

        filter_list = list()
        for category_id in range(len(self.categories)):
            cat_total_size = category_size_list[category_id]
            cat_filter_list = [0]*(cat_total_size - category_max_size) + [1]*(category_max_size)
            random.shuffle(cat_filter_list)
            filter_list = filter_list + cat_filter_list
        
        return self.NewDataset_Filter(filter_list)

        #     category_list = list()
        #     file_list = list()
        #     for idx in range(len(filter_list)):
        #         if(1 == filter_list[idx]):
        #             category_list.append(self.category_list[idx])
        #             file_list.append(self.file_list[idx])
        
        # return file_list, category_list
        # return Dataset(categories)

class Datasetzoid:
    def __init__(self, path_to, categories, path_from):
        self.categories = categories
        self.categoryzoid = Categoryzoid(categories)
        
        self.category_list = list()
        self.file_list = list()
        self.file_source_list = list()
        self._Generate(path_to, path_from)
        # self.dataset = False
        
    def _Generate(self, path_to, path_from):
        file_list = MP3FileList(path_from)
        self.categoryzoid.Classify(file_list, delete_filtered=True, verbose=True)
        
        try:
            shutil.rmtree(path_to)
        except OSError as e:
            pass
            # print("Error: %s : %s" % (path_to, e.strerror))
        
        os.mkdir(path_to)
        categories, categories_file_match = self.categoryzoid.CopyGenerate(path_to)
        
        self.category_list = list()
        self.file_list = list()
        self.file_source_list = list()
        for category_id in range(len(categories)):
            for file in categories[category_id]:
                self.category_list.append(category_id)
                self.file_list.append(file)
                self.file_source_list.append(categories_file_match[category_id])
        
        # self.dataset = Dataset(self.categories, self.category_list, self.file_list, self.file_source_list)
    
    def _SamplesGetUpToCategorySize(self, category_max_size):
        total_size = len(self.category_list)
        filter_list = [0]*(total_size - max_size) + [1]*(max_size)
        random.shuffle(filter_list)
        
        category_list = list()
        file_list = list()
        for idx in range(len(filter_list)):
            if(1 == filter_list[idx]):
                category_list.append(self.category_list[idx])
                file_list.append(self.file_list[idx])
        
        return file_list, category_list

    def _SamplesGetUpToSize(self, max_size):
        total_size = len(self.category_list)
        filter_list = [0]*(total_size - max_size) + [1]*(max_size)
        random.shuffle(filter_list)
        
        category_list = list()
        file_list = list()
        for idx in range(len(filter_list)):
            if(1 == filter_list[idx]):
                category_list.append(self.category_list[idx])
                file_list.append(self.file_list[idx])
        
        return file_list, category_list

    def Dataset(self):
        dataset = Dataset(self.categories, self.category_list, self.file_list, self.file_source_list)
        return dataset

    def SamplesGet(self, max_size = False):
        if(max_size):
            return self._SamplesGetUpToSize(category_max_size)
        else:
            return self.file_list, self.category_list